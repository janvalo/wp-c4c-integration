<?php

namespace C4CIntegration\Client;

/**
 * API Client Class for SOAP Requests
 */
class APIClient extends \SoapClient
{
  private $defaultLocation = null;
  private $loginDetails;
  public $request;

  function __construct($wsdl, $location, $options=[])
  {

    $this->defaultLocation = $location;
    $this->loginDetails = [];

    use_soap_error_handler(true);
    parent::__construct($wsdl, array_merge($this->loginDetails, $options));
  }
	function setXMLStr($value)
	{
		$this->request = $value;
	}

	function SoapClientCall($SOAPXML)
	{
		return $this->setXMLStr($SOAPXML);
	}

  function __doRequest($request, $location, $action, $version, $one_way = NULL)
  {

    $request = $this->request;
    $dom = new \DOMDocument('1.0');
    try {
      $dom->loadXML($request);
    }

    catch(\DOMException $e) {
      die($e->code);
    }

    $request = $dom->saveXML();

    // Read the language from the configuration file
    $language = 'EN';

    if (!empty($language)) {
      $location  = $location . '&sap-language=' . $language;
    }

    //Consuming the web service
    $soapResponse = parent::__doRequest($request, $location, $action, $version);

    if (empty($soapResponse))
    {
      throw new \Exception($this->__soap_fault->faultcode . ": Cannot connect to host: $location. Reason:" . $this->__soap_fault->getMessage());
    }

    return $soapResponse;
  }
}
?>