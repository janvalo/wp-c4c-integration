<?php

namespace C4CIntegration\Client;


class FeedSoap extends \SoapClient
{
	var $XMLStr = "";
	function setXMLStr($value)
	{
		$this->XMLStr = $value;
	}

	function getXMLStr()
	{
		return $this->XMLStr;
	}

	function __doRequest($request, $location, $action, $version, $one_way = NULL)
	{
		$request = $this->XMLStr;
		$dom = new \DOMDocument('1.0');
		try {
			$dom->loadXML($request);
		}

		catch(DOMException $e) {
			die($e->code);
		}

		$request = $dom->saveXML();

		// doRequest

		return parent::__doRequest($request, $location, $action, $version);
	}

	function SoapClientCall($SOAPXML)
	{
		return $this->setXMLStr($SOAPXML);
	}
}