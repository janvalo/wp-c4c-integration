<?php

namespace C4CIntegration\Actions;

use C4CIntegration\Client\APIClient;
use C4CIntegration\Client\FeedSoap;



class ManageMarketingLeadIn
{

  public $wsdlPath;
  public $endpoint;
  public $isProxy = false;
  public $auth = [];
  

  public function __construct($params = [])
  {

    $schema = isset( $params['is_test']) ? 'ManageMarketingLeadIn_test' : 'ManageMarketingLeadIn';
		$this->wsdlPath = dirname(__FILE__).'/../Schema/'.$schema.'.wsdl';
		$this->endpoint = $params['endpoint'] ?? 'https://my312690.crm.ondemand.com/sap/bc/srt/scs/sap/managemarketingleadin?sap-vhost=my312690.crm.ondemand.com';
    $this->auth = [
       'verify_peer' => false,
       'allow_self_signed' => true,
       'connection_timeout' => 300,

      'trace' => 1,
      "exceptions" => 1,
      'login'      => $params['login'],
      'password'   => $params['password'],
      // 'soap_version' => SOAP_1_2,
    ];

  }

  public function execute($payload)
  {
    $request = $this->generateXml($payload);
    $result  = $this->create($request);
    $doc = new \DOMDocument();
    $doc->loadXML($result);
    
    $id = $result->MarketingLead->UUID->_ ?? NULL;
    // print_r([$id,$result, ]);
    return $id;
	}

  public function create($soapVar)
  {

    $client = new APIClient($this->wsdlPath,$this->endpoint, $this->auth);

    $client->__setLocation($this->endpoint);

		try {

      $client->SoapClientCall($soapVar);
      $result = $client->MaintainBundle();

      return $result;
      
		} catch(Exception $e) { 
			$message = $e->getMessage(); 
		}
	}

  public function generateSoapVar($xml)
  {
		try { 
			$soapVar = new \SoapVar($xml, XSD_ANYXML, null, null, null); 
			return $soapVar;
		} catch(\Exception $e) { 
			$message = $e->getMessage(); 
		}
	}

	public function generateXml($payload) {
		$xml = '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:glob="http://sap.com/xi/SAPGlobal20/Global" xmlns:a12="http://sap.com/xi/AP/CustomerExtension/BYD/A12SK">
					<soap:Header/>
					<soap:Body>
       
            <glob:MarketingLeadBundleMaintainRequest_sync>
              <MarketingLead actionCode="01" itemListCompleteTransmissionIndicator="true" >
                <a12:CSAName></a12:CSAName>
                <Name>DLG</Name>
                <Note>Web Form Lead</Note>
                <PersonFirstName>'.$payload['FirstName'].'</PersonFirstName>
                <PersonLastName>'.$payload['LastName'].'</PersonLastName>
                <CompanyName>'.$payload['CompanyName'].'</CompanyName>
                <OriginTypeCode>002</OriginTypeCode>
                <Address>
                  <CountryCode>'.$payload['Country'].'</CountryCode>
                  <RegionCode></RegionCode>
                  <StreetPostalCode>'.$payload['PostCode'].'</StreetPostalCode>
                  <CityName>'.$payload['City'].'</CityName>
                  <StreetName>'.$payload['Address'].'</StreetName>
                  <TelephoneFormattedNumberDescription>'.$payload['ContactPhone'].'</TelephoneFormattedNumberDescription>
                  <EmailURI>'.$payload['Email'].'</EmailURI>
                </Address>
                <a12:Market>'.$payload['Market'].'</a12:Market>
                <a12:Category>'.$payload['Category'].'</a12:Category>
                <a12:Source>'.$payload['Source'].'</a12:Source>

              </MarketingLead>
            </glob:MarketingLeadBundleMaintainRequest_sync>
					</soap:Body>
        </soap:Envelope>';

		return $xml;		
	}

}

?>