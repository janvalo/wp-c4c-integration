# SAP C4C Integration

## Installation

Copy this folder to the `wp-content/plugins` directory and then go to admin panel -> Plugins and activate **_WP C4C Form Integrate_**

For the front end form there are dependencies that you will be asked to install. The front end form is created using **_Ninja Forms_**

### Setting up the form

Once all required plugins are installed and active, you will need to import the form by uploading `nf_form_Marketing_Leads.nff` via admin panel in Ninja Forms -> Import/Export

## Configuration

You will need to configure API access credentials in SAP C4C Options menu in the admin panel.

## Emails and reposnses

To manage email templates and notification, go to Ninja Forms and edit the `Marketing Lead` Form. The API calls are done in the background at the point of teh form submission.
