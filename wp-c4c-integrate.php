<?php
/**
 * Plugin Name: WP C4C Form Integrate
 * Description: SAP C4C Marketing Lead Form Integration
 * Version: 1.0.4
 * Author: Source Town <dev@source.run>
 */

// autoload dependencies
require_once( 'vendor/autoload.php' );
require_once('src/dependencies.php');


ini_set('soap.wsdl_cache_enabled', 0);
ini_set('soap.wsdl_cache_ttl', 0);

use Carbon_Fields\Container;
use Carbon_Fields\Field;
use C4CIntegration\Actions\ManageMarketingLeadIn;


add_action( 'carbon_fields_register_fields', function () {
    Container::make( 'theme_options', __( 'SAP C4C Options' ) )
        ->add_tab( __( 'General' ), array(

            Field::make( 'text', 'crb_email_from', __( 'Email From' )),

            Field::make( 'checkbox', 'crb_enable_live', __( 'Enable Live Mode' ) )
            ->set_option_value( 'yes' ),

            Field::make( 'text', 'crb_test_api_url', 'Test API Endpoint' )
                ->set_conditional_logic( array(
                    array(
                        'field' => 'crb_enable_live',
                        'value' => false,
                    )
            ) ),
            Field::make( 'text', 'crb_test_api_login', 'Test API Login' )
                ->set_conditional_logic( array(
                    array(
                        'field' => 'crb_enable_live',
                        'value' => false,
                    )
            ) ),
            Field::make( 'text', 'crb_test_api_pasword', 'Test API Password' )
                ->set_conditional_logic( array(
                    array(
                        'field' => 'crb_enable_live',
                        'value' => false,
                    )
            ) ),

            Field::make( 'text', 'crb_live_api_url', 'Live API Endpoint' )
                ->set_conditional_logic( array(
                    array(
                        'field' => 'crb_enable_live',
                        'value' => true,
                    )
            ) ),
            Field::make( 'text', 'crb_live_api_login', 'Live API Login' )
                ->set_conditional_logic( array(
                    array(
                        'field' => 'crb_enable_live',
                        'value' => true,
                    )
            ) ),
            Field::make( 'text', 'crb_live_api_pasword', 'Live API Password' )
                ->set_conditional_logic( array(
                    array(
                        'field' => 'crb_enable_live',
                        'value' => true,
                    )
            ))
        )
    );
});

add_action( 'after_setup_theme', function () {
    \Carbon_Fields\Carbon_Fields::boot();
});


function get_api_creds() {
    $check_live = carbon_get_theme_option( 'crb_enable_live' );
    $creds = [
        'endpoint' => carbon_get_theme_option('crb_test_api_url'),
        'login' => carbon_get_theme_option('crb_test_api_login'),
        'password' => carbon_get_theme_option('crb_test_api_pasword'),
        'is_test' => true
    ];
    if ($check_live == true) {
        $creds = [
            'endpoint' => carbon_get_theme_option('crb_live_api_url'),
            'login' => carbon_get_theme_option('crb_live_api_login'),
            'password' => carbon_get_theme_option('crb_live_api_pasword')
        ];
    }
    return $creds;
}

function do_test_call() {

    $creds = get_api_creds();

    $lead = new ManageMarketingLeadIn($creds);
    $result = $lead->execute([
                'FirstName'     => 'Source',
                'LastName'      => 'Runner',
                'CompanyName'   => 'Test Ltd',
                'Country'       => 'UK',
                'PostCode'      => 'SW1Y 5ED',
                'City'          => 'London',
                'Address'       => '123 Pall Mall',
                'ContactPhone'  => '01234567890',
                'Email'         => 'dev@source.run',
                'Market'        => 151,
                'Category'      => 101,
                'Source'        => 'DLG',
    ]);
    print_r($result);
}


add_action( 'init', function() {
    if( isset( $_GET['test_call'] ) ) {
        do_test_call();
    }
});

add_action( 'ninja_forms_after_submission', 'my_ninja_forms_after_submission' );

function my_ninja_forms_after_submission( $form_data )
{
    
    $map = [
        'first_name' => 'FirstName',
        'last_name' => 'LastName',
        'company_name' => 'CompanyName',
        'country' => 'Country',
        'postcode' => 'PostCode',
        'city' => 'City',
        'address' => 'Address',
        'phone' => 'ContactPhone',
        'email' => 'Email',
    ];

    $payload = [

        'FirstName'     => $form_data['first_name'],
        'LastName'      => $form_data['last_name'],
        'CompanyName'   => $form_data['company_name'],
        'Country'       => $form_data['country'],
        'PostCode'      => $form_data['postcode'],
        'City'          => $form_data['city'],
        'Address'       => $form_data['address'],
        'ContactPhone'  => $form_data['phone'],
        'Email'         => $form_data['email'],
        'Market'        => 151,
        'Category'      => 101,
        'Source'        => 'DLG',

    ];

    foreach( $form_data[ 'fields' ] as $field ) {
        if( in_array($field[ 'key' ], array_keys($map)) ) {
            $payload[$map[$field['key']]] = $field['value'];
        }
    }

    $creds = get_api_creds();

    $lead = new ManageMarketingLeadIn($creds);
    $result = $lead->execute($payload);
    if(!is_null($result)) {

        return true;
    }
}


// apply_filters( 'ninja_forms_run_action_settings', $action[ 'settings' ], $this->_form_id, $action[ 'id' ], $this->_form_data['settings'] )

add_filter( 'ninja_forms_run_action_settings', function( $settings, $form_id, $action_id, $form_settings ) {
    
    // Only filter the email action.
    if( 'email' == $settings[ 'type' ] ) {
    
        // Override the from address.
        if(strlen(carbon_get_theme_option('crb_email_from')) > 0) $settings[ 'from_address' ] = carbon_get_theme_option('crb_email_from');
    }
    
    // Remember to return the (maybe) filtered settings.
    return $settings;
}, 10, 4 );

add_action('wp_head', 'nf_custom_styles', 100);

function nf_custom_styles()
{
 echo "<style>.nf-input-limit {display: none !important}</style>";
}